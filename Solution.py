import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split


iris_url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"



def scaling(scaler, data):
    data = scaler.fit_transform(data)
    return data

df = pd.read_csv(iris_url, names=['sepallength', 'sepalwidth', 'petallength', 'petalwidth', 'species'])
df.iloc[:, :-1] = scaling(StandardScaler(), df.iloc[:, :-1])
sns.pairplot(df)
# plt.show()








url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'Class']
dataset = pd.read_csv(url, names=names)

X = dataset.drop('Class', 1)
y = dataset['Class']

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

from sklearn.preprocessing import StandardScaler

sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

classifier = RandomForestClassifier(max_depth=2, random_state=0)
classifier.fit(X_train, y_train)

# Predicting the Test set results
y_pred = classifier.predict(X_test)

from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
print("All features:")
cm = confusion_matrix(y_test, y_pred)
print(cm)
print('Accuracy=' + str(accuracy_score(y_test, y_pred)))




# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

from sklearn.decomposition import PCA
X_train_before = X_train.copy()
pca = PCA(n_components=1)
X_train = pca.fit_transform(X_train)
X_test = pca.transform(X_test)

explained_variance = pca.explained_variance_ratio_

classifier = RandomForestClassifier(max_depth=2, random_state=0)
classifier.fit(X_train, y_train)

# Predicting the Test set results
y_pred = classifier.predict(X_test)


print("PC1:")
cm = confusion_matrix(y_test, y_pred)
print(cm)
print('Accuracy=' + str(accuracy_score(y_test, y_pred)))